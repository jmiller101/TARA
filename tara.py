import csv
import glob
import os

DATA_LOCATION = 'data'
OUTPUT_LOCATION = 'output'
COMBINED_NAME = 'combined.csv'
PROCESSED_NAME = 'processed.csv'

OUTPUT_VARIABLES = [
    'Positive Climate',
    'Teacher Sensitivity',
    'Regard for Adolescent Perspectives',
    'Behavior Management',
    'Productivity',
    'Negative Climate',
    'Instructional Learning Formats',
    'Content Understanding',
    'Analysis and Inquiry',
    'Quality of Feedback',
    'Instructional Dialogue',
    'Student Engagement'
]

VARIABLE_MAP = {
    'Positive Climate': 'PC',
    'Teacher Sensitivity': 'TS',
    'Regard for Adolescent Perspectives': 'RAP',
    'Behavior Management': 'BM',
    'Productivity': 'PR',
    'Negative Climate': 'NC',
    'Instructional Learning Formats': 'ILF',
    'Content Understanding': 'CU',
    'Analysis and Inquiry': 'AI',
    'Quality of Feedback': 'QF',
    'Instructional Dialogue': 'ID',
    'Student Engagement': 'Eng'
}

DOMAIN_MAP = {
    'ES': [
        'PC',
        'TS',
        'RAP'
    ],
    'CO': [
        'BM',
        'PR',
        'NC'
    ],
    'IS': [
        'ILF',
        'CU',
        'AI',
        'QF',
        'ID'
    ]
}

GENERATED_VARIABLES = [
    'Rater D1_R1',
    'Rater D1_R2',
    'Rater D2_R1',
    'Rater D2_R2',
    'D1 Video ID',
    'D2 Video ID',
    'Cohort',
    'Subject'
]

SEGMENT_LABELS = [
    'D1_R1_S1',
    'D1_R1_S2',
    'D1_R2_S1',
    'D1_R2_S2',
    'D2_R1_S1',
    'D2_R1_S2',
    'D2_R2_S1',
    'D2_R2_S2'
]

AVERAGE_LABELS = [
    'Avg_D1_R1',
    'Avg_D1_R2',
    'Change_D1',
    'Avg_D1',
    'Avg_D2_R1',
    'Avg_D2_R2',
    'Change_D2',
    'Avg_D2',
    'Avg_SCORE'
]


def create_csv_file(location, name, data, fields):
    combined_path = os.path.abspath(os.path.join(os.getcwd(), location, name))
    with open(combined_path, 'w', newline='', encoding='utf-8') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fields)
        writer.writeheader()
        writer.writerows(data)


def add_error_to_row(row, new_error):
    try:
        old_errors = row['Errors'] + '; '
    except KeyError:
        old_errors = ''
    row['Errors'] = old_errors + new_error


class Processor:
    def __init__(self, csv_data, identifying_info):
        self._video_rows = {}
        self._output_rows = []

        self._generate_output_fields()

        for row in csv_data:
            self._process_segments(row)

        for key in self._video_rows:
            row = self._video_rows[key]
            self._process_days(row)
            self._process_class(row)
            self._process_domains(row)
            self._add_identifying_info(row, identifying_info)

        for key in self._video_rows:
            video_row = self._video_rows[key]
            output_row = {}
            for field in video_row:
                output_row[field] = video_row[field]
            self._output_rows.append(output_row)

        try:
            create_csv_file(OUTPUT_LOCATION, PROCESSED_NAME, self._output_rows, GENERATED_VARIABLES)
        except ValueError as err:
            split_err = str(err).split('\'')[1::2]
            print('\n\n')
            print('Column labels in error:')
            print(str(split_err) + '\n\n')
            print('Rows(s) found with column labels:')
            count = 0
            for row in self._output_rows:
                for field in split_err:
                    if field in row:
                        count += 1
                        print(str(row) + '\n\n')
                        break
            print(str(count) + ' issue(s) found')

    def _process_segments(self, raw_row):
        video_id = raw_row['Video ID']

        split_id = list(map(str.strip, video_id.split('_')))
        if len(split_id) is not 3:
            print('The video ID \"' + video_id + '\" is not valid')
            return
        wave_cohort = split_id[0] + '_' + split_id[1]
        day = split_id[2]

        if wave_cohort in self._video_rows:
            out_row = self._video_rows[wave_cohort]
        else:
            out_row = {}
        out_row['D' + day + ' Video ID'] = video_id
        out_row['Cohort'] = split_id[1]

        # Figuring out whether the segment is from the first or second rater for a given day
        if 'Rater D' + day + '_R1' in out_row.keys() and raw_row['Rater'] != out_row['Rater D' + day + '_R1']:
            rater = 'D' + day + '_R2'
        else:
            rater = 'D' + day + '_R1'
        out_row['Rater ' + rater] = raw_row['Rater']

        for variable in OUTPUT_VARIABLES:
            abbreviation = VARIABLE_MAP[variable]
            segment_root = abbreviation + '_' + rater + '_S'

            if not raw_row['Segment Number']:
                add_error_to_row(out_row, 'One of the segment numbers for ' + abbreviation + '_' + rater +
                                 ' was not listed')
                print(raw_row)
                break

            current_segment = segment_root + raw_row['Segment Number']

            out_row[current_segment] = raw_row[variable]
            self._combine_segments(out_row, segment_root, rater)

        self._video_rows[wave_cohort] = out_row

    @staticmethod
    def _generate_output_fields():
        GENERATED_VARIABLES.append('ES_domain_Avg')
        GENERATED_VARIABLES.append('CO_domain_Avg')
        GENERATED_VARIABLES.append('IS_domain_Avg')
        for variable in OUTPUT_VARIABLES:
            abbreviation = VARIABLE_MAP[variable]
            for average in AVERAGE_LABELS:
                GENERATED_VARIABLES.append(abbreviation + '_' + average)
        for variable in OUTPUT_VARIABLES:
            abbreviation = VARIABLE_MAP[variable]
            for segment in SEGMENT_LABELS:
                GENERATED_VARIABLES.append(abbreviation + '_' + segment)
        GENERATED_VARIABLES.append('Errors')

    @staticmethod
    def _combine_segments(out_row, segment_root, rater):
        variable = segment_root.split('_')[0]

        segment_1 = segment_root + '1'
        segment_2 = segment_root + '2'
        if segment_1 not in out_row.keys() and segment_2 not in out_row.keys():
            return

        try:
            value_1 = float(out_row[segment_1])
            value_2 = float(out_row[segment_2])
            video_average = (value_1 + value_2) / 2

            if abs(value_1 - value_2) > 3:
                add_error_to_row(out_row, 'Difference between segments for variable "' + variable + '" with rater ' +
                                 rater + ' was greater than 3')

            out_row[variable + '_Avg_' + rater] = "{0:.2f}".format(video_average)
        except KeyError:
            return
        except ValueError:
            return

    @staticmethod
    def _process_days(day_row):
        for day in range(1, 3):
            for abbreviation in VARIABLE_MAP.values():
                rater_1_column = abbreviation + '_Avg_D' + str(day) + '_R1'
                rater_2_column = abbreviation + '_Avg_D' + str(day) + '_R2'
                day_change_column = abbreviation + '_Change_D' + str(day)
                day_average_column = abbreviation + '_Avg_D' + str(day)

                if rater_1_column in day_row:
                    rater_1_value = float(day_row[rater_1_column])
                else:
                    add_error_to_row(day_row, 'This video has no day ' + str(day) + ' raters')
                    break

                if rater_2_column in day_row:
                    rater_2_value = float(day_row[rater_2_column])
                elif day < 2:
                    add_error_to_row(day_row, 'This video is missing day 1, rater 2\'s average')
                    break
                else:
                    day_row[day_average_column] = "{0:.2f}".format(rater_1_value)
                    continue

                day_change = abs(rater_1_value - rater_2_value)
                if day_change > 2:
                    add_error_to_row(day_row, 'Difference between raters was greater than 2 for "' + abbreviation + '"')
                day_average = (rater_1_value + rater_2_value) / 2

                day_row[day_change_column] = day_change
                day_row[day_average_column] = "{0:.2f}".format(day_average)

    @staticmethod
    def _process_class(class_row):
        for abbreviation in VARIABLE_MAP.values():
            day_1_column = abbreviation + '_Avg_D1'
            day_2_column = abbreviation + '_Avg_D2'
            dimension_column = abbreviation + '_Avg_SCORE'

            if day_1_column in class_row:
                day_1_value = float(class_row[day_1_column])
            else:
                return

            if day_2_column in class_row:
                day_2_value = float(class_row[day_2_column])
            else:
                class_row[dimension_column] = day_1_value
                continue

            dimension_average = (day_1_value + day_2_value) / 2
            class_row[dimension_column] = "{0:.2f}".format(dimension_average)

    @staticmethod
    def _process_domains(domain_row):
        for domain in DOMAIN_MAP.keys():
            domain_average_column = domain + '_domain_Avg'
            total = 0
            count = 0
            for dimension in DOMAIN_MAP[domain]:
                dimension_average_column = dimension + '_Avg_SCORE'
                if dimension_average_column in domain_row:
                    total += float(domain_row[dimension_average_column])
                    count += 1
            if count > 0:
                domain_avg = total / count
                domain_row[domain_average_column] = "{0:.2f}".format(domain_avg)

    @staticmethod
    def _add_identifying_info(row, info):
        cohort = row['Cohort']
        try:
            row['Subject'] = info[cohort]
        except KeyError:
            row['Subject'] = 'Not provided'


class Combiner:
    def __init__(self):
        self.csv_data = []
        self.identifying_info = {}
        self._fieldnames = ['Rater']

        self._load_files()

        self._fieldnames.append('Errors')
        self._validate_raw_data(self.csv_data)
        create_csv_file(OUTPUT_LOCATION, COMBINED_NAME, self.csv_data, self._fieldnames)

    def _load_files(self):
        csv_files = glob.glob(DATA_LOCATION + '/*.csv')
        for file_path in csv_files:
            with open(file_path, newline='') as csv_file:
                reader = csv.DictReader(csv_file)
                if 'identifying_info' in file_path:
                    for row in reader:
                        self.identifying_info[row['Cohort']] = row['Subject']
                else:
                    for field_name in reader.fieldnames:
                        if field_name not in self._fieldnames:
                            self._fieldnames.append(field_name)
                    for row in reader:
                        row['Rater'] = self._get_name_from_file(file_path)
                        self.csv_data.append(row)

    @staticmethod
    def _validate_raw_data(rows):
        for row in rows:
            for variable in OUTPUT_VARIABLES:
                try:
                    raw_value = int(row[variable])
                    if raw_value < 1:
                        add_error_to_row(row, 'The value of variable "' + variable + '" is less than 1')

                    if raw_value > 7:
                        add_error_to_row(row, 'The value of variable "' + variable + '" is greater than 7')

                    if variable == 'Negative Climate' and raw_value > 3:
                        add_error_to_row(row, 'The value of variable "' + variable + '" is greater than 3')
                except ValueError:
                    if not row[variable]:
                        add_error_to_row(row, 'This segment is not finished being coded')
                        break
                    else:
                        add_error_to_row(row, 'The value of variable "' + variable + '" is not a number: "' +
                                         row[variable] + '"')

    @staticmethod
    def _get_name_from_file(file_path):
        filename = os.path.basename(file_path)
        return filename.split('_')[0]


def main():
    combiner = Combiner()
    Processor(combiner.csv_data, combiner.identifying_info)


if __name__ == "__main__":
    main()
